﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotifyDatabaseService
{
    public class Message
    {
        private int id;
        private string content;
        //private DeliveryStatus status;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        public string Content
        {
            get { return content; }
            set { content = value; }
        }
        //public DeliveryStatus Status
        //{
        //    get { return status; }
        //    set { status = value; }
        //}
    }

    public enum DeliveryStatus
    {
        Delivered = 1,
        NotDelivered = 2
    }
}
