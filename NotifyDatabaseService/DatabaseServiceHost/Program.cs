﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseServiceHost
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ServiceHost host = new ServiceHost(typeof(NotifyDatabaseService.DatabaseService)))
            {
                host.Open();
                Console.WriteLine("Database service started..");

                Console.ReadLine();
            }
        }
    }
}
