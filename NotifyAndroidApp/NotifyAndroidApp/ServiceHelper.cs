using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.ServiceModel;

namespace NotifyAndroidApp
{
    public static class ServiceHelper
    {
        private static readonly EndpointAddress EndPoint = new EndpointAddress("http://192.168.8.101:8080/UnifyService");

        public static UnifyServiceClient InitializeHelloWorldServiceClient()
        {
            BasicHttpBinding binding = CreateBasicHttp();

            return new UnifyServiceClient(binding, EndPoint);
        }

        private static BasicHttpBinding CreateBasicHttp()
        {

            BasicHttpBinding binding = new BasicHttpBinding
            {
                Name = "basicHttpBinding",
                MaxBufferSize = 2147483647,
                MaxReceivedMessageSize = 2147483647
            };
            TimeSpan timeout = new TimeSpan(0, 0, 30);
            binding.SendTimeout = timeout;
            binding.OpenTimeout = timeout;
            binding.ReceiveTimeout = timeout;
            return binding;
        }
    }
}