using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using System.Collections.Generic;
using SupportFragment = Android.Support.V4.App.Fragment;
using Message = NotifyDatabaseService.Message;
using NotifyDatabaseService;

namespace NotifyAndroidApp.Fragments
{
    public class Fragment3 : SupportFragment

    {
        private RecyclerView mRecyclerView;
        private RecyclerView.LayoutManager mLayoutManager;
        public RecyclerView.Adapter mAdapter;
        private MyList<Message> mMsg;
        private UnifyServiceClient _client;

        public override void OnCreate(Bundle savedInstanceState)
        {
            
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            mRecyclerView = inflater.Inflate(Resource.Layout.Fragment3, container, false) as RecyclerView;

            _client = ServiceHelper.InitializeHelloWorldServiceClient();

            mMsg = new MyList<Message>();

            _client.getLastMessagesCompleted += _client_getLastMessagesCompleted;
            _client.getLastMessagesAsync(MainActivity.logedUser.Id);

            mLayoutManager = new LinearLayoutManager(mRecyclerView.Context);
            mRecyclerView.SetLayoutManager(mLayoutManager);
            mAdapter = new RecyclerAdapter(mMsg);
            mMsg.Adapter = mAdapter;
            mRecyclerView.SetAdapter(mAdapter);
            
            
            return mRecyclerView;
        }
        

        private void _client_getLastMessagesCompleted(object sender, getLastMessagesCompletedEventArgs e)
        {
            
            Message[] msgs = e.Result;
            System.Console.WriteLine("msg count - "+msgs.Length);
            foreach(Message msg in msgs)
            {
                System.Console.WriteLine("send to add - " + msg.GroupName);
                System.Console.WriteLine("send to add - " + msg.Content);
                System.Console.WriteLine("send to add - " + msg.ID);
                mMsg.Add(msg);
            }
        }

        public class MyList<T>
        {
            private List<T> mItems;
            private RecyclerView.Adapter mAdapter;

            public MyList()
            {
                mItems = new List<T>();
            }

            public RecyclerView.Adapter Adapter
            {
                get { return mAdapter; }
                set { mAdapter = value; }
            }

            public void Add(T item)
            {
                mItems.Add(item);

                if (Adapter != null)
                {
                    Adapter.NotifyItemInserted(0);
                }
            }

            public T this[int index]
            {
                get { return mItems[index]; }
                set { mItems[index] = value; }
            }

            public int Count
            {
                get { return mItems.Count; }
            }
        }

        public class RecyclerAdapter : RecyclerView.Adapter
        {
            private MyList<Message> mMsg;

            public RecyclerAdapter(MyList<Message> msgs)
            {
                mMsg = msgs;
            }

            public class MyView : RecyclerView.ViewHolder
            {
                public View mMainView { get; set; }
                public TextView mGroupName { get; set; }
                public TextView mMsgContent { get; set; }
                public ImageView mImage { get; set; }

                public MyView(View view) : base(view)
                {
                    mMainView = view;
                }
            }

            public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View row = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.ReceivedMsgRow, parent, false);

                TextView txtGroupName = row.FindViewById<TextView>(Resource.Id.txtReceivedMsgGroupName);
                TextView txtMsgContent = row.FindViewById<TextView>(Resource.Id.txtReceivedMsgContent);
                ImageView image = row.FindViewById<ImageView>(Resource.Id.receivedMsgimage);


                MyView view = new MyView(row) { mGroupName = txtGroupName, mMsgContent = txtMsgContent, mImage = image };
                return view;
            }

            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                MyView myHolder = holder as MyView;
                int indexPosition = (mMsg.Count - 1) - position;
                System.Console.WriteLine("msg gname - "+ mMsg[indexPosition].GroupName);
                myHolder.mGroupName.Text = mMsg[indexPosition].GroupName;
                myHolder.mMsgContent.Text = mMsg[indexPosition].Content;
                //should get image according to group name
                myHolder.mImage.SetImageResource(ImageResolver.getGroupImage(mMsg[indexPosition].GroupName));
                
            }

            public override int ItemCount
            {
                get { return mMsg.Count; }
            }
        }
    }
}