using System;
using Android.App;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using SupportFragment = Android.Support.V4.App.Fragment;
using System.Collections.Generic;
using Android.Graphics;
using Android.Util;
using Android.Content;
using Android.Content.Res;
using Android.Widget;
using NotifyDatabaseService;

namespace NotifyAndroidApp.Fragments
{
    public class Fragment1 : SupportFragment
    {
        private RecyclerView mRecyclerView;
        private RecyclerView.LayoutManager mLayoutManager;
        private RecyclerView.Adapter mAdapter;
        private MyList<Group> mGroup;
        private UnifyServiceClient _client;
        

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            mRecyclerView = inflater.Inflate(Resource.Layout.Fragment1, container, false) as RecyclerView;

            _client = ServiceHelper.InitializeHelloWorldServiceClient();

            //mEmails = new List<Email>();
            //mEmails.Add(new Email() { Name = "tom", Subject = "Wanna hang out?", Message = "I'll be around tomorrow!!" });
            mGroup = new MyList<Group>();
            //mGroup.Add(new Group() { Id = 1, Name = "test name", AtName = "@Test_namek"});
            _client.getGroupsCompleted += _client_getGroupsCompleted;
            _client.getGroupsAsync(MainActivity.logedUser.Id);

            mLayoutManager = new LinearLayoutManager(mRecyclerView.Context);
            mRecyclerView.SetLayoutManager(mLayoutManager);
            mAdapter = new RecyclerAdapter(mGroup,mRecyclerView,this);
            mGroup.Adapter = mAdapter;
            mRecyclerView.SetAdapter(mAdapter);


            return mRecyclerView;
        }

        private void _client_getGroupsCompleted(object sender, getGroupsCompletedEventArgs e)
        {
            Group[] groups = e.Result;
            foreach(Group group in groups)
            {
                mGroup.Add(group);
            }
        }

        public class MyList<T>
        {
            private List<T> mItems;
            private RecyclerView.Adapter mAdapter;

            public MyList()
            {
                mItems = new List<T>();
            }

            public RecyclerView.Adapter Adapter
            {
                get { return mAdapter; }
                set { mAdapter = value; }
            }

            public void Add(T item)
            {
                mItems.Add(item);

                if(Adapter != null)
                {
                    Adapter.NotifyItemInserted(0);
                }
            }

            public T this[int index]
            {
                get { return mItems[index]; }
                set { mItems[index] = value; }
            }

            public int Count
            {
                get { return mItems.Count; }
            }
        }


        public class RecyclerAdapter : RecyclerView.Adapter
        {
            private MyList<Group> mGropu;
            private RecyclerView mRecyclerView;
            private Fragment1 main;

            public RecyclerAdapter(MyList<Group> groups, RecyclerView recyclerView, Fragment1 con)
            {
                mGropu = groups;
                mRecyclerView = recyclerView;
                main = con;
            }

            public class MyView : RecyclerView.ViewHolder
            {
                public View mMainView { get; set; }
                public TextView mName { get; set; }
                public TextView mAtName { get; set; }
                public ImageView mImage { get; set; }

                public MyView(View view) : base(view)
                {
                    mMainView = view;
                }
            }

            public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View row = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.row, parent, false);

                TextView txtName = row.FindViewById<TextView>(Resource.Id.txtName);
                TextView txtAtName = row.FindViewById<TextView>(Resource.Id.txtAtName);
                ImageView image = row.FindViewById<ImageView>(Resource.Id.image);


                MyView view = new MyView(row) { mName = txtName, mAtName = txtAtName, mImage = image};
                return view;
            }

            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                MyView myHolder = holder as MyView;
                
                int indexPosition = (mGropu.Count - 1) - position;
                myHolder.mName.Text = mGropu[indexPosition].GroupName;
                myHolder.mAtName.Text = mGropu[indexPosition].At_name;
                //should get image according to group name
                myHolder.mImage.SetImageResource(ImageResolver.getGroupImage(mGropu[indexPosition].Id));
                myHolder.mMainView.Click += MMainView_Click;
            }

            private void MMainView_Click(object sender, EventArgs e)
            {
                int position = mRecyclerView.GetChildLayoutPosition((View)sender);
                //int position = mRecyclerView.GetChildPosition((View)sender);
                //int indexPosition = (mGropu.Count - 1) - position;
                Group cGroup = mGropu[position];
                System.Console.WriteLine("message name" + mGropu[position].Id);
                Intent intent = new Intent(main.Context, typeof(SendMessage));
                intent.PutExtra("groupId", cGroup.Id);
                intent.PutExtra("groupName", cGroup.GroupName);
                main.StartActivity(intent);

            }

            public override int ItemCount
            {
                get { return mGropu.Count; }
            }
        }

    }
}