using Android;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using System.Collections.Generic;
using SupportFragment = Android.Support.V4.App.Fragment;
using Message = NotifyDatabaseService.Message;

namespace NotifyAndroidApp.Fragments
{
    public class Fragment2 : SupportFragment
    {

        private RecyclerView mRecyclerView;
        private RecyclerView.LayoutManager mLayoutManager;
        private RecyclerView.Adapter mAdapter;
        private SendMsgList<Message> mMsg;
        private UnifyServiceClient _client;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            mRecyclerView = inflater.Inflate(Resource.Layout.Fragment2, container, false) as RecyclerView;


            //mEmails = new List<Email>();
            //mEmails.Add(new Email() { Name = "tom", Subject = "Wanna hang out?", Message = "I'll be around tomorrow!!" });
            mMsg = new SendMsgList<Message>();
            _client = ServiceHelper.InitializeHelloWorldServiceClient();
            //get send messages event


            mLayoutManager = new LinearLayoutManager(mRecyclerView.Context);
            mRecyclerView.SetLayoutManager(mLayoutManager);
            mAdapter = new RecyclerAdapter(mMsg,mRecyclerView);
            mMsg.Adapter = mAdapter;
            mRecyclerView.SetAdapter(mAdapter);

            return mRecyclerView;
        }

        public class SendMsgList<T>
        {
            private List<T> mItems;
            private RecyclerView.Adapter mAdapter;

            public SendMsgList()
            {
                mItems = new List<T>();
            }

            public RecyclerView.Adapter Adapter
            {
                get { return mAdapter; }
                set { mAdapter = value; }
            }

            public void Add(T item)
            {
                mItems.Add(item);

                if (Adapter != null)
                {
                    Adapter.NotifyItemInserted(0);
                }
            }

            public T this[int index]
            {
                get { return mItems[index]; }
                set { mItems[index] = value; }
            }

            public int Count
            {
                get { return mItems.Count; }
            }
        }

        public class RecyclerAdapter : RecyclerView.Adapter
        {
            private SendMsgList<Message> mMsg;
            private RecyclerView mRecyclerView;

            public RecyclerAdapter(SendMsgList<Message> msgs, RecyclerView recyclerView)
            {
                mMsg = msgs;
                mRecyclerView = recyclerView;
            }

            public class MyView : RecyclerView.ViewHolder
            {
                public View mMainView { get; set; }
                public TextView mGroupName { get; set; }
                public TextView mMsgContent { get; set; }
                public ImageView mImage { get; set; }

                public MyView(View view) : base(view)
                {
                    mMainView = view;
                }
            }

            public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View row = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.SentMsgRow, parent, false);

                TextView txtGroupName = row.FindViewById<TextView>(Resource.Id.txtSentMsgGroupName);
                TextView txtMsgContent = row.FindViewById<TextView>(Resource.Id.txtSentMsgContent);
                ImageView image = row.FindViewById<ImageView>(Resource.Id.sentMsgimage);


                MyView view = new MyView(row) { mGroupName = txtGroupName, mMsgContent = txtMsgContent, mImage = image };
                return view;
            }

            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                MyView myHolder = holder as MyView;
                int indexPosition = (mMsg.Count - 1) - position;
                myHolder.mGroupName.Text = mMsg[indexPosition].GroupName;
                myHolder.mMsgContent.Text = mMsg[indexPosition].GroupName;
                //should get image according to group name
                myHolder.mImage.SetImageResource(Resource.Drawable.api);
                myHolder.mMainView.Click += MMainView_Click;
            }

            private void MMainView_Click(object sender, System.EventArgs e)
            {
                int position = mRecyclerView.GetChildLayoutPosition((View)sender);
                int indexPosition = (mMsg.Count - 1) - position;
                System.Console.WriteLine("message name"+mMsg[indexPosition].ID);
            }

            public override int ItemCount
            {
                get { return mMsg.Count; }
            }
        }
    }
}