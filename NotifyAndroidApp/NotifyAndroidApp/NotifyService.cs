using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using NotifyDatabaseService;
using Message = NotifyDatabaseService.Message;
using System.Threading;

namespace NotifyAndroidApp
{
    [Service]
    public class NotifyService : IntentService
    {
        public static event Action<Message> MsgReceived;
        private UnifyServiceClient _client;
        private int userId;

        protected override void OnHandleIntent(Intent intent)
        {
            _client = ServiceHelper.InitializeHelloWorldServiceClient();
            userId = intent.GetIntExtra("userId", 0);
            _client.checkMsgCompleted += _client_checkMsgCompleted;
            while (true)
            {
                Thread.Sleep(10000);
                _client.checkMsgAsync(userId);
                //testMethod();
            }
        }

        private void _client_checkMsgCompleted(object sender, checkMsgCompletedEventArgs e)
        {
            if(e.Result != null)
            {
                MsgReceived(e.Result);
            }
        }

        private void testMethod()
        {
            MsgReceived(new Message() { ID = 1, Content = "new msg", GroupName = "APIDevelopment" });
        }
    }
}