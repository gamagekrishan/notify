using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.Design.Widget;
using Message = NotifyDatabaseService.Message;

namespace NotifyAndroidApp
{
    [Activity(Label = "SendMessage", Theme = "@style/Theme.DesignDemo")]
    public class SendMessage : Activity
    {
        Button btnSend;
        Button btnCancel;
        TextInputLayout txtMsg;
        UnifyServiceClient _client;
        TextView txtHead;
        int groupId;
        string groupName;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.SendMessage);

            _client = ServiceHelper.InitializeHelloWorldServiceClient();

            btnSend = FindViewById<Button>(Resource.Id.btnSend);
            btnCancel = FindViewById<Button>(Resource.Id.btnSendMsgCancel);
            txtMsg = FindViewById<TextInputLayout>(Resource.Id.txtSendMsgContent);
            txtHead = FindViewById<TextView>(Resource.Id.txtSendMsgTitle);
            

            groupId = Intent.GetIntExtra("groupId",0);
            groupName = Intent.GetStringExtra("groupName");
            btnSend.Click += BtnSend_Click;
            btnCancel.Click += BtnCancel_Click;
            //send message
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Finish();
        }

        private void BtnSend_Click(object sender, EventArgs e)
        {
            Message msg = new Message();
            msg.GroupName = groupName;
            msg.Content = txtMsg.EditText.Text;
            _client.sendMsgAsync(6, MainActivity.logedUser.Id, msg);
            Finish();
        }
    }
}