﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using DatabaseServiceConsumer.DatabaseService;

namespace UnifyService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IUnifyService" in both code and config file together.
    [ServiceContract]
    public interface IUnifyService
    {

        [OperationContract]
        User login(string userName, string password);

        [OperationContract]
        List<Message> getLastMessages(int userId);

        [OperationContract]
        List<Group> getGroups(int userId);

        [OperationContract]
        Message checkMsg(int userId);

        [OperationContract]
        void sendMsg(int groupId,int senderId,Message msg);

        [OperationContract]
        void sendMsgFromWeb(int groupId, Message msg);

    }
}
