﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseServiceConsumer.DatabaseService;

namespace DatabaseServiceConsumer
{
    public class Dao
    {
        DatabaseService.DatabaseServiceClient _client = new DatabaseService.DatabaseServiceClient("BasicHttpBinding_IDatabaseService");

        public User getUserByUserName(string userName)
        {
            return _client.getUserByUserName(userName);
        }

        public List<Group> getGroupsByUserId(int userId)
        {
            return _client.getGroupsByUserId(userId);
        }

        public List<Message> getLastMessages(int userId)
        {
            return _client.getFewMessages(userId);
        }

        public void sendMessage(int groupId, int senderId, Message msg)
        {
            Console.WriteLine("gId -"+groupId+" senderId - "+senderId+"msg - "+msg.Content);
            _client.saveMessage(senderId, groupId, msg);
            Console.WriteLine("msg send");
        }

        public List<User> getUsersOfGroup(int groupId)
        {
            return _client.getUsersOfGroup(groupId);
        }

        
    }
}
