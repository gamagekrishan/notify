﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace UnifyServiceHost
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ServiceHost host = new ServiceHost(typeof(UnifyService.UnifyService)))
            {
                host.Open();
                Console.WriteLine("Unify service started..");

                Console.ReadLine();
            }
        }
    }
}
