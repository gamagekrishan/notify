﻿using NotifySMSService.DatabaseService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NotifySMSService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class SMSService : ISMSServiceHost
    {
        SMSServiceDao aDao = new SMSServiceDao();

        SMSHandler smsHandler = new SMSHandler();
        public void sendMessage(int senderId, DatabaseService.Message msg, int groupId)
        {
            List<User> users = aDao.getUsersOfGroup(groupId);
            foreach(User user in users)
            {
                smsHandler.sendMsg(null, "COM1", "1000", user.PNo, msg.ToString());
            }
            
        }

        public void deliverMessage(int senderId, DatabaseService.Message msg, int groupId)
        {
            throw new NotImplementedException();
        }
    }
}
