﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NotifySMSService.DatabaseService;

namespace NotifySMSService
{
    public class SMSServiceDao
    {
        private DatabaseServiceClient dbClient= new DatabaseServiceClient("WSHttpBinding_IDatabaseService");

        public User getUser(string userName)
        {
            User user = dbClient.getUserByUserName(userName);
            return user;
        }

        public List<Group> getGroups(int userId)
        {
            return dbClient.getGroupsByUserId(userId);
        }

        public List<User> getUsersOfGroup(int groupId)
        {
            return dbClient.getUsersOfGroup(groupId);
        }

        public void saveMessage(int senderId, int groupId, Message msg)
        {
            dbClient.saveMessage(senderId, groupId, msg);
        }
    }
}
